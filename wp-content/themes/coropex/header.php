<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<link rel="profile" href="https://gmpg.org/xfn/11">
		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>

		<?php
		wp_body_open();
		?>

		<div class="header" id="top">
			<div class="container-xl">
			  	<div class="container">
			    	<nav class="navbar navbar-expand-lg">
			      		<a class="navbar-brand" href="<?php echo site_url();?>">
			        		<div class="logo">
			        			<?php 
			        			$custom_logo_id = get_theme_mod( 'custom_logo' );
								$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
								$image_url = $image ? $image[0] : 'https://dummyimage.com/274x98/000/fff.png&text=Not+Found';
								?>
			          			<img src="<?php echo $image_url;?>" class="img img-fluid">
			        		</div>
			      		</a>
				      	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
					        <span class="navbar-toggler-icon"></span>
				    	</button>
			      		
				      	<div class="collapse navbar-collapse" id="collapsibleNavbar">
							<?php 
				      		$defaults = array(
								'theme_location'  => '',
								'menu'            => '',
								'container'       => '',
								'container_class' => '',
								'container_id'    => '',
								'menu_class'      => 'menu',
								'menu_id'         => '',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'before'          => '',
								'after'           => '',
								'link_before'     => '',
								'link_after'      => '',
								'items_wrap'      => '<ul class="navbar-nav ml-auto">%3$s</ul>',
								'depth'           => 0,
								'walker'          => ''
							);
							 
							wp_nav_menu( $defaults );
					    	?>
					    </div>
			    	</nav>
			  	</div>
			</div>
		</div>
		<?php
		$add_inner_class = is_front_page() ? "" : "inner-bg";
		?>
		<div class="landing-bg <?php echo $add_inner_class;?>">
			<img src="<?php echo get_the_post_thumbnail_url();?>" class="img img-fluid">
		</div>