<?php
/**
 * Template Name: Homepage Template
 * Template Post Type: post, page
*/

get_header();
?>
<div class="landing-area">
    <div class="container-xl">
      	<div class="container">
        	<div class="landing-content">
          		<div class="lc-1 wow fadeInUp">
      				<?php 
          			the_post();
          			the_content();
          			?>
          		</div>
	          	<div class="lc-2">
		            <a href="#contact-form-content" class="btn-lc">Request a Quote</a>
		        </div>
        	</div>
      	</div>
    </div>
</div>


<div class="values-wrapper" id="values">
    <div class="container">
      	<div class="center-header common-header">
	        <h4>Values</h4>
	        <img src="<?php echo get_template_directory_uri();?>/assets/images/head-b.png" class="img img-fluid">
	    </div>

      	<div class="row">
      		<?php
		    while ( have_rows('values') ) : the_row();
		        ?>
		  		<div class="col-6 col-lg-3">
	          		<div class="row">
	            		<div class="value-content">
		              		<img src="<?php the_sub_field('icon');?>" class="img img-fluid value-icn">
		              		<h4><?php the_sub_field('title');?></h4>
		              		<?php the_sub_field('content');?>
		              		<div class="overlay-value"></div>
		          			<img src="<?php the_sub_field('image');?>" class="img img-fluid value-bg">
	        			</div>
	          		</div>
	        	</div>
		        <?php
		    endwhile;
			?>
      	</div>
    </div>
</div>

<div class="service-wrapper">
    <div class="container">
      	<div class="center-header common-header">
        	<h4>SERVICES</h4>
        	<img src="<?php echo get_template_directory_uri();?>/assets/images/head-b.png" class="img img-fluid">
      	</div>
      	<ul>
      		<?php
			while ( have_rows('services') ) : the_row();
			    ?>
				<li class="wow fadeInUp"><img src="<?php the_sub_field('image');?>">
	          		<h4><?php the_sub_field('title');?></h4>
	          		<?php the_sub_field('content');?>
	        	</li>
			    <?php
			endwhile;
			?>
      	</ul>
    </div>
</div>


<div class="who_we_are_wrapper" id="about_us">
    <div class="container">
    	<?php
    	$wwr_image_url = get_field('who_we_are_image') ? get_field('who_we_are_image') : 'https://dummyimage.com/660x650/000/fff.png&text=Not+Found';
    	?>
      	<ul>
        	<li><img src="<?php echo $wwr_image_url;?>" class="img img-fluid"></li>
	        <li>
	          <h4>Who we are?</h4>
	          <?php echo get_field('who_we_are')?>
	        </li>
      	</ul>
    </div>
</div>

<div class="who_we_are_wrapper what_we_wrapper">
    <div class="container">
    	<?php 
    	$wwd_image_url = get_field('what_we_do_image') ? get_field('what_we_do_image') : "https://dummyimage.com/660x650/000/fff.png&text=Not+Found";
    	?>
      	<ul>
        	<li>
          		<div>
            		<h4>What we do?</h4>
            		<?php echo get_field('what_we_do');?>
          		</div>
        	</li>
        	<li><img src="<?php echo $wwd_image_url;?>" class="img img-fluid"></li>
      	</ul>
    </div>
</div>

<div class="why_choose_wrapper">
	<div class="container">
	  	<div class="center-header common-header">
	    	<h4>Why Choose us?</h4>
	        <p class="header-bottom"> 
	        	We are one stop solution for<br>all your packaging requirements.
	        </p>
	  	</div>


	  	<div class="row">
	  		<?php
		    while ( have_rows('why_choose_us') ) : the_row();
		        ?>
		  		<div class="col-lg-3">
	          		<div class="item-content">
	            		<?php the_sub_field('content');?>
	          		</div>
	        	</div>
		        <?php
		    endwhile;
			?>
        </div>
	</div>
</div>

<div class="manufacture_wrapper">
	<div class="container">
      	<div class="common-header center-header">
        	<h4>What do we manufacture?</h4>
        	<img src="<?php echo get_template_directory_uri();?>/assets/images/head-b.png" class="img img-fluid">
      	</div>
  		<div class="manu-grid">
    		<div class="row">
				<?php
			    while ( have_rows('what_do_we_manufacture?') ) : the_row();
			        ?>
			  		<div class="col-lg-4">
			            <div class="manu_div wow fadeInUp" >
			              	<img src="<?php the_sub_field('image');?>" class="img img-fluid">
			              	<div class="manu-text">
			                	<h3><?php the_sub_field('title');?></h3>
			                	<h5><?php the_sub_field('sub_title');?></h5>
			                	<?php the_sub_field('content');?>
			              	</div>
			            </div>
	      			</div>
			        <?php
			    endwhile;
				?>
    		</div>
  		</div>
      	<div class="competetive-wrapper">
        	<div class="row">
	          	<div class="col-lg-6">
		            <div class="competetive-left">
		              	<h1>Competitive<br>
		                Advantage</h1>
		              	<?php echo get_field('competitive_advantage_left');?>
		            </div>
	          	</div>
          		<div class="col-lg-6">
            		<div class="competetive-right">
            			<ul>
	            			<?php
						    while ( have_rows('competitive_advantage_right') ) : the_row();
						        ?>
								<li>
									<img src="<?php the_sub_field('image');?>" class="img img-fluid">
				                  	<span>
				                    	<h4><?php the_sub_field('title');?></h4>
				                    	<?php the_sub_field('content');?>
				                  	</span>
				                </li>
						        <?php 
						    endwhile;
							?>
						</ul>
            		</div>
          		</div>
        	</div>
      	</div>
    </div>
</div>

<div class="quality-wrapper">
  <div class="container">
    <div class="common-header center-header">
        <h4>Quality Standards</h4>
        <img src="<?php echo get_template_directory_uri();?>/assets/images/head-b.png" class="img img-fluid">
    </div>
    <div class="row">
    	<div class="col-lg-6">
          	<h5>ISO Packaging Manufacturers</h5>
			<?php the_field("iso_packaging_manufacturers");?>
      	</div>
      	<div class="col-lg-6">
          	<h5>ISTA</h5>
          	<b>INTERNATIONAL SAFE TRANSIT ASSOCIATION</b>
			<?php the_field("ista");?>
      	</div>
  	</div>
  </div>
</div>

<?php 
get_sidebar('contact');
get_footer();
?>