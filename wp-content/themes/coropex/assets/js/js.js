$(document).ready(function() {
  $('.owl-why').owlCarousel({
    loop: true,
    margin: 40,
    nav: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 2
      },
      1600: {
        items: 3
      }
    }
  })



  new WOW().init();
});
$(window).scroll(function() {
  var scroll = $(window).scrollTop();

  if (scroll >= 1000) {
    $(".backtop").addClass("op1");
  } else {
    $(".backtop").removeClass("op1");
  }
});
