<div class="contact-wrapper ">
	<div class="container">
	  	<div class="wwd-content" id="contact-form-content">
	    	<div class="w-100">
	      		<div class="row">
			        <div class="col-lg-6">
			          	<div class="contact-info">
			            	<h3>Let’s talk</h3>
			            	<p>Ask us anything or just<br>
			              	say Hi…</p>
			            	<ul>
			              		<li><img src="<?php echo get_template_directory_uri();?>/assets/images/phone.svg" class="img img-fluid"> <span><a href="tel:<?php echo get_field('phone_number','option');?>"><?php echo get_field('phone_number','option');?></a></span></li>
			              		<li><img src="<?php echo get_template_directory_uri();?>/assets/images/email.svg" class="img img-fluid"> <span><a href="mailto:<?php echo get_field('email_address','option');?>"><?php echo get_field('email_address','option');?></a></span></li> 
			              		<li><img src="<?php echo get_template_directory_uri();?>/assets/images/location.svg" class="img img-fluid"> <span><?php echo get_field('address_1','option');?></span></li> 
			              		<li style="margin-left: 54px;"><span><?php echo get_field('address_2','option');?></span></li>
			            	</ul>
			          	</div>
			        </div>
			        <div class="col-lg-6">
			          	<div class="contact-form">
			          		<?php echo do_shortcode('[contact-form-7 id="85" title="Contact form 1"]');?>
						</div>
			        </div>
	      		</div>
	   		</div>
	  	</div>
	</div>
</div>