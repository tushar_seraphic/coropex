<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */
$footer_logo = get_field('footer_logo','option');
$footer_logo_url = $footer_logo ? $footer_logo : 'https://dummyimage.com/390x60/000/fff.png&text=Not+Found';
?>
		<footer>
		    <div class="container">
		      	<div class="inner-footer">
		        	<div>
		        		<img src="<?php echo $footer_logo_url;?>" class="img img-fluid foot-logo">
		        		<span>
		        			Copyright © <?php echo date('Y');?> <?php echo get_bloginfo();?> All rights reserved.
		        		</span>
		        	</div>
		        	<ul>
		          		<li><a href="<?php echo get_field('twitter_link','option');?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/twitter.svg" class="img img-fluid"></a></li>
		          		<li><a href="<?php echo get_field('instagram_link','option');?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/insta.svg" class="img img-fluid"></a></li>
		          		<li><a href="<?php echo get_field('facebook_link','option');?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/fb.svg" class="img img-fluid"></a></li>
		        	</ul>
		      	</div>
		    </div>
	  	</footer> 
	  	<div class="container">
		    <a href="#top" class="backtop"><img src="<?php echo get_template_directory_uri();?>/assets/images/backtop.png" class="img img-fluid"></a>
	  	</div>
		<?php wp_footer(); ?>

	</body>
</html>
