<?php
/**
 * Template Name: Products Template
 * Template Post Type: post, page
*/

get_header();
?>
<div class="product-listing-wrapper">
    <div class="container">
      	<div class="common-header center-header">
       		<h4>Products</h4>
        	<img src="<?php echo get_template_directory_uri();?>/assets/images/head-b.png" class="img img-fluid">
      	</div>
      	<div class="row">
			<?php
		  	$type = 'product';
			$args = array(
				'post_type' => $type,
			    'post_status' => 'publish',
			    'posts_per_page' => -1,
			    'ignore_sticky_posts'=> true
		  	);
			$my_query = null;
			$my_query = new WP_Query($args);
			?>

			<?php 
			if( $my_query->have_posts() ):
		  		while ($my_query->have_posts()) : $my_query->the_post(); ?>
			  	<div class="col-lg-4">
		          	<div class="product-inner-wrapper">
		          		<?php
				    	$pro_image_url = get_the_post_thumbnail_url() ? get_the_post_thumbnail_url() : 'https://dummyimage.com/420x160/000/fff.png&text=Not+Found';
				    	?>
		            	<img src="<?php echo $pro_image_url;?>" class="img img-fluid">
	            		<div class="piw-div">
	              			<h4><?php the_title();?></h4>
              				<?php the_content();?>
	            		</div>
		          	</div>
		        </div>
		  		<?php endwhile; ?>
			<?php
			endif;
			wp_reset_query();
			?>
      	</div>
    </div>
</div>

<?php /*<div class="retail-wrapper">
    <div class="container">
      	<div class="center-header common-header">
	        <h4>RETAIL PACKAGING OPTIONS</h4>
        	<img src="<?php echo get_template_directory_uri();?>/assets/images/head-b.png" class="img img-fluid">
      	</div>

      	<div class="row">
	        <?php
			while ( have_rows('retail_packaging_options') ) : the_row();
		    	$retail_image_url = get_sub_field('image') ? get_sub_field('image') : 'https://dummyimage.com/420x160/000/fff.png&text=Not+Found';
			    ?>
				<div class="col-lg-4">
	          		<div class="retail-div">
	            		<img src="<?php echo $retail_image_url;?>" class="img img-fluid">
	            		<h4><?php the_sub_field('title');?></h4>
	            		<b><?php the_sub_field('sub_title');?></b>
	            		<?php the_sub_field('content');?>
	          		</div>
	        	</div>
			    <?php
			endwhile;
			?>
      	</div>
    </div>
</div>*/?>

<div class="landing-area inner-quote">
  	<div class="container">
        <div class="landing-content">
          	<div class="lc-1 wow fadeInUp">
            	<p>Let us help you create the solution for your packaging needs.</p>
          	</div>
          	<div class="lc-2">
            	<a href="#contact-form-content" class="btn-lc">Request a Quote</a>
          	</div>
        </div>
    </div>
</div>

<?php 
get_sidebar('contact');
get_footer();
?>